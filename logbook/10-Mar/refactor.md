# Refactoring `pair_solver`

Split `main` into multiple files to decongest it - deprecated previously written functions (eg `find_unique_pairs`) now put into their own files.
