import codecs
import os.path

from setuptools import find_packages, setup


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name="maxwellmunchkin",
    version=get_version("pair_solver/__init__.py"),
    description="solver for minimising intra-pair distances",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/maxwellmunchkin/research-project",
    packages=find_packages(),
    author="W. McCorkindale & R. Elijošius",
    license="MIT License",
)
