# Methodology

## Choice of data structure

The nature of the problem we wish to solve is symmetric with respect to swapping of the particles within a pair, as well as to the ordering of the different pairs in the final solution. This is the motivation for our choice of representing the solution output as a `set` (order invariant) of pairs, each pair being a sorted `tuple` (permutation invariant).

## Workflow

A flowchart of the `pair_solver` implementation is shown below:

<div style="text-align: center;">

```mermaid
flowchart TB
Coords["Particle coordinates\nnp.ndarray 2NxD"] --> Dim(D==1?)
Dim --Yes--> C["Sort the particles"] --> Solution("Solution\nset(sorted(tuples))")
Dim --No--> D["Project to 1D"]
D --> C
Coords --> Dist["Calculate loss\nfloat"]
Solution --> Dist
Dist --> Dim2(D==1?)
Dim2 --yes--> Return("Return answer\nset(pairs), loss")
Dim2 --no--> Initial(Start from Initial Guess)

subgraph Monte-Carlo Optimizer
Initial --> step(step>=budget?)
step --no--> pick(Choose two random pairs)
pick --> swap(Swap two particles from each pair)
swap --> delta(Calculate Delta in loss)
delta --> criteria("delta<0 \nor\n  np.exp(-1 * delta * beta) > np.random.rand() ?")
criteria --yes--> accept(accept step \n and update solution)
criteria --no--> no_accept(reject step)
accept --> step
no_accept --> step
end
step --yes--------> Return("Return answer\nset(pairs), loss")

```
</div>
