# Developer's Guide

## 1. Introduction and goals

The software within this project implements a solver for finding the pairs within 2N particles that minimises the distance between them. Below we describe the general development approach for the project.

More detailed discussion can be found in the [details](details.md) file.

### 1.1. Requirements

- The software must return the analytical solution in 1D, and otherwise return an approximate solution within a 30 sec time limit for ~100 particles.
- The code must be covered by unit tests.

### 1.2. Quality goals

1. Functional Suitability
2. Reliability
3. Performance and Efficiency

## 2. Context and scope

This library aims to solve the problem of pair distance minimisation which is a general mathematical problem applicable to many fields (eg logistics, physics simulations) so the codebase should be as domain-agnostic as possible.

The scope of the solver is just to return the solution and the final objective function given an input of particle coordinates, and optional optimisation parameters specified by the user. The use of external mathematical libaries is encouraged when they are needed.

## 3. Solution strategy

The problem of finding the exact solution scales exponentially with the number of particles, thus we employ approximate solutions.

In the current release, a monte-carlo based optimisation of an initial guess via PCA is implemented. The initial guess is a projection of the particles to 1D where we use the fact that an analytical solution (sorting) exists for the 1D case.

## 4. Architecture Decisions

The main architectural question for this project is the use of class vs functions. Here, we decided not to use a class for the solver as all the necessary information to solve the problem can be convienently stored as a distance matrix.
