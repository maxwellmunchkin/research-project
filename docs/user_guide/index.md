# User Guide

## Installation

In the root directory of the repo, run `pip install ./` to install the package and use our functions in other code!

## Quickstart

To use our algorithm, specify the coordinates of an even number of particles and pass them as an argument into `pair_solver.mc_solution` to obtain the particle pairings, as well as the value of the loss function under this pairing.

```python
import pair_solver 

solution, loss = pair_solver.mc_solution(particle_coordinates)
```

where `particle_coordinates` is a `numpy` array of $(2N, M)$ particle coordinate. The pairings corresponding to the approximate solution are stored in `solution` as a `set` of sorted `tuple`s. `loss` is the corresponding value of the sum of intra-pair distances.

Addtional arguments can be passed into `mc_solution` to control the degree of optimisation done:

| name | type | default| description |
| :--- | :--- | :--- | :--- |
| `budget` | `int` | 1000 | How many Monte-Carlo iterations to run. |
| `beta_init` | `float` | 1.0 | Starting Boltzmann factor $\beta$. Low factor (ie lower temperature) leads to more accepted changes. |
| `beta_max` | `float` | 100.0 | Maximum allowed Boltzmann factor $\beta$ used for simulated annealing. |
| `num_logs` | `int` | 5 | The number of logs to be printed during optimisation - the frequency of logging will be evenly distributed over the course of the optimisation. |

Further details on the effect of these hyperparameter values on the resultant solution can be found in the publication.

## Example

Below is a simple demonstration of how to use the package. The code can be run directly in a jupyter notebook [here](https://gitlab.com/maxwellmunchkin/research-project/blob/final-release/docs/user_guide/example.ipynb).

Let's start with the simple 1D case:

```python
import numpy as np

import pair_solver

coords_1d = np.random.random((4, 1))
solution_1d, loss_1d = pair_solver.mc_solution(coords_1d)

print(coords_1d)
print(solution_1d)
```

```
For a 1D case, the optimal solution is sorting.
Returning the 1D analytical solution.
[[0.37454012]
 [0.95071431]
 [0.73199394]
 [0.59865848]]
{(1, 2), (0, 3)}
```

What about in 2D?

```python
coords_2d = np.random.random((4, 2))
solution_2d, loss_2d = pair_solver.mc_solution(coords_2d)

print(coords_2d)
print(solution_2d)
```


```
Step: 200, acc. ratio: 0.305
Step: 400, acc. ratio: 0.152
Step: 600, acc. ratio: 0.102
Step: 800, acc. ratio: 0.076
Ran for 1000 steps, with acc. ratio: 0.061
Initial loss: 0.82, new_loss: 0.82
Final beta: 100.9
[[0.15601864 0.15599452]
 [0.05808361 0.86617615]
 [0.60111501 0.70807258]
 [0.02058449 0.96990985]]
{(0, 2), (1, 3)}
```
![Example plot in 2D](solution_in_2D.png)

The implementation scales well with number of particles:

```python
import time

n_particles = 1000
n_dimension = 2
start_time = time.perf_counter()
coords_many_particles = np.random.random((n_particles, n_dimension))
solution_many_particles, loss_many_particles = pair_solver.mc_solution(coords_many_particles)
end_time = time.perf_counter()

print(f'Time taken for {n_particles} particles in {n_dimension} dimensions: {end_time - start_time:.2f}s')
```

```
Step: 200, acc. ratio: 0.395
Step: 400, acc. ratio: 0.290
Step: 600, acc. ratio: 0.248
Step: 800, acc. ratio: 0.233
Ran for 1000 steps, with acc. ratio: 0.221
Initial loss: 159.61, new_loss: 130.86
Final beta: 100.9
Time taken for 1000 particles in 2 dimensions: 20.08s
```

as well as dimension:

```python
n_particles = 1000
n_dimension = 10
start_time = time.perf_counter()
coords_high_dimension = np.random.random((n_particles, n_dimension))
solution_many_dimension, loss_many_dimension = pair_solver.mc_solution(coords_high_dimension)
end_time = time.perf_counter()

print(f'Time taken for {n_particles} particles in {n_dimension} dimensions: {end_time - start_time:.2f}s')
```

```
Step: 200, acc. ratio: 0.565
Step: 400, acc. ratio: 0.425
Step: 600, acc. ratio: 0.363
Step: 800, acc. ratio: 0.341
Ran for 1000 steps, with acc. ratio: 0.323
Initial loss: 601.71, new_loss: 539.02
Final beta: 100.9
Time taken for 1000 particles in 10 dimensions: 20.20s
```

The max number of particles that can be run in 30s if we fix budget = $100\times n_{particles}$ is ~450

```python
n_particles = 400
budget_for_convergence = 100 * n_particles
n_dimension = 3
start_time = time.perf_counter()
coords_high_dimension = np.random.random((n_particles, n_dimension))
solution_many_dimension, loss_many_dimension = pair_solver.mc_solution(coords_high_dimension, budget=budget_for_convergence)
end_time = time.perf_counter()

print(f'Time taken for {n_particles} particles in {n_dimension} dimensions: {end_time - start_time:.2f}s')
```

```
Step: 9000, acc. ratio: 0.062
Step: 18000, acc. ratio: 0.038
Step: 27000, acc. ratio: 0.028
Step: 36000, acc. ratio: 0.022
Ran for 45000 steps, with acc. ratio: 0.0188
Initial loss: 116.71, new_loss: 28.75
Final beta: 100.9
Time taken for 450 particles in 3 dimensions: 28.07s
```