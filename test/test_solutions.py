import numpy as np
import pytest
from scipy.spatial import distance_matrix
from pair_solver import main, monte_carlo


@pytest.mark.parametrize(
    "test_coords, solver",
    # fmt: off
    [
        pytest.param(np.random.random(size=(3,3)) ,
                     main.random_solution, 
                    marks=pytest.mark.xfail(
                    raises=ValueError, reason="Requires even number of particles"
                     ),
                     id='random_odd_error'),
        pytest.param(np.random.random(size=(3,3)) ,
                     main.pca_solution, 
                    marks=pytest.mark.xfail(
                    raises=ValueError, reason="Requires even number of particles"
                     ),
                     id='pca_odd_error'),
        pytest.param(np.random.random(size=(3,3)) ,
                     monte_carlo.mc_solution, 
                    marks=pytest.mark.xfail(
                    raises=ValueError, reason="Requires even number of particles"
                     ),
                     id='mc_odd_error'),
    ],
    # fmt: on
)
def test_odd_particles_raise_error(test_coords, solver):
    _, out_loss = solver(test_coords)


@pytest.mark.parametrize(
    "test_coords, solver",
    # fmt: off
    [
        pytest.param(np.random.random(size=(100,3)) ,
                     main.random_solution, 
                     id='random_loss_validity'),
        pytest.param(np.random.random(size=(100,3)) ,
                     main.pca_solution, 
                     id='pca_loss_validity'),
        pytest.param(np.random.random(size=(100,3)) ,
                     monte_carlo.mc_solution, 
                     id='mc_loss_validity'),
    ],
    # fmt: on
)
def test_valid_loss(test_coords, solver):
    sq_distance = lambda x, y: np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = main.generate_matrix(test_coords, sq_distance)

    num_pairs = test_coords.shape[0] / 2
    min_dist = np.min(interaction_matrix[np.nonzero(interaction_matrix)]) * num_pairs
    max_dist = np.max(interaction_matrix) * num_pairs
    _, out_loss = solver(test_coords)
    assert min_dist <= out_loss <= max_dist


@pytest.mark.parametrize(
    "test_coords, solver",
    # fmt: off
    [
        pytest.param(np.random.random(size=(100,3)) ,
                     main.random_solution, 
                     id='random_solution_validity'),
        pytest.param(np.random.random(size=(100,3)) ,
                     main.pca_solution, 
                     id='pca_solution_validity'),
        pytest.param(np.random.random(size=(100,3)) ,
                     monte_carlo.mc_solution, 
                     id='mc_solution_validity'),
    ],
    # fmt: on
)
def test_valid_solution(test_coords, solver):
    num_pairs = test_coords.shape[0] / 2
    sol, _ = solver(test_coords)
    particles_in_sol = []
    for pair in sol:
        particles_in_sol.extend(list(pair))
    assert num_pairs == len(particles_in_sol) / 2 and set(
        [i for i in range(len(particles_in_sol))]
    ) == set(particles_in_sol)


def test_mc_loss_calculation():
    coords = np.random.random(size=(200, 5))
    sol, loss = monte_carlo.mc_solution(coords)

    sq_distance = lambda x, y: np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = main.generate_matrix(coords, sq_distance)

    full_loss = main.evaluate_solution(interaction_matrix, sol)

    np.testing.assert_almost_equal(loss, full_loss)


def test_mc_one_d_behavior():
    coords = np.random.random(size=(200, 1))
    pca_sol, pca_loss = main.pca_solution(coords)
    mc_sol, mc_loss = monte_carlo.mc_solution(coords)

    assert mc_sol == pca_sol
    assert pca_loss == mc_loss
