import numpy as np
import pytest

from pair_solver import monte_carlo
from pair_solver.main import generate_matrix


@pytest.mark.parametrize(
    "test_pair, test_swap, expected_set",
    # fmt: off
    [
        pytest.param({(0, 1), (2, 3)}, {1 ,2}, {(0, 2), (1, 3)}, id='two_pair_1d_pairing_A'),
        pytest.param({(0, 1), (2, 3)}, {3 , 0}, {(1, 3), (0, 2)}, id='two_pair_1d_pairing_B'),
        pytest.param({(0, 1), (2, 3)}, {0 , 1}, {(0, 1), (2, 3)}, id='two_pair_1d_redundant'),
    ],
    # fmt: on
)
def test_swap(test_pair, test_swap, expected_set):
    assert monte_carlo.swap_particles_between_pairs(
        test_pair, test_swap) == expected_set


@pytest.mark.parametrize(
    "test_coords, test_pair, test_swap, expected_delta",
    # fmt: off
    [
        pytest.param(np.asarray([0, 1, 2, 3]), {(0, 1), (2, 3)}, {1 ,2}, 
                     (2, {(0, 2), (1, 3)}), 
                     id='two_pair_1d_pairing_A'),
        pytest.param(np.asarray([0, 1, 3, 10]), {(0, 1), (2, 3)}, {1 ,2}, 
                     (4, {(0, 2), (1, 3)}), 
                     id='two_pair_1d_pairing_B'),

    ],
    # fmt: on
)
def test_evaluate_delta(test_coords, test_pair, test_swap, expected_delta):
    def sq_distance(x, y): return np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = generate_matrix(test_coords, sq_distance)
    assert (
        monte_carlo.return_delta_in_loss(
            interaction_matrix, test_pair, test_swap)
        == expected_delta
    )


@pytest.mark.parametrize(
    "test_soln, test_old, test_new, expected_soln",
    # fmt: off
    [
        pytest.param({(0, 1), (2, 3), (4, 5)}, {(0, 1), (2, 3)}, {(0, 2), (1, 3)}, 
                     {(0, 2), (1, 3), (4, 5)}, 
                     id='three_pairs_1d_pairing_A'),
        pytest.param({(0, 1), (2, 3), (4, 5)},  {(2, 3), (4, 5)}, {(2, 4), (3, 5)}, 
                     {(0,1), (2, 4), (3, 5)}, 
                     id='three_pairs_1d_pairing_B'),
    ],
    # fmt: on
)
def test_update_pairing(test_soln, test_old, test_new, expected_soln):
    assert monte_carlo.update_pairing(
        test_soln, test_old, test_new) == expected_soln
