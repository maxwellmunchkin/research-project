from time import perf_counter

import numpy as np
import pytest
from pair_solver.one_d_case import solution_in_1d, project_to_1d


@pytest.mark.parametrize(
    "test_coords,expected_pairs",
    # fmt: off
    [
        pytest.param([], set(tuple()), 
                     id='zero_pairs'),
        pytest.param([0,1], set(((0, 1),)), 
                     id='one_pair'),
        pytest.param([0, 1, 2, 3], set(((0, 1), (2, 3))), 
                     id='two_pairs_ordered'),
        pytest.param([0, 10, 2, 3], set(((0, 2), (1, 3))), 
                     id='two_pairs_unordered'),  
        pytest.param([0, 10, 2, 3, -1, 0], [set(((0, 4), (2, 5), (1, 3),)), 
                                            set(((0, 2), (4, 5), (1, 3),))], 
                     id='three_pairs'),        

    ],
    # fmt: on
)
def test_solution_in_1d(test_coords, expected_pairs):
    if isinstance(expected_pairs, list):  # degenerate solutions passed as a list
        predicted_pairs = solution_in_1d(test_coords)
        assert np.any([predicted_pairs == pair for pair in expected_pairs])
    else:
        assert solution_in_1d(test_coords) == expected_pairs


@pytest.mark.parametrize(
    "test_coords,expected_proj",
    # fmt: off
    [
        pytest.param(np.array([[1]]), None, 
                     marks=pytest.mark.xfail(raises=ValueError, reason='Only one point provided'), 
                    id='single_point'),
        pytest.param(np.array([1, 2, 3, 4]), np.array([1, 2, 3, 4]), 
                     id='1d_already'),
        pytest.param(np.array([[1, 0], [2, 0]]), np.array([-0.5, 0.5]), 
                     id='2d'),
        pytest.param(np.array([[1, 0, 0], [2, 0, 0]]), [np.array([0.5, -0.5]),
                                                        np.array([-0.5, 0.5])],
                     id='3d'),     
    ],
    # fmt: on
)
def test_project_to_1d(test_coords, expected_proj):
    if isinstance(expected_proj, list):  # degenerate solutions passed as a list
        predicted_proj = project_to_1d(test_coords)
        assert np.any([np.allclose(predicted_proj, proj)
                      for proj in expected_proj])
    else:
        np.testing.assert_allclose(project_to_1d(test_coords), expected_proj)


def test_1d_speed():
    inp = np.random.random(size=(1000, 3))
    start = perf_counter()
    projected = project_to_1d(inp)
    solved = solution_in_1d(projected)
    end = perf_counter()
    assert end - start <= 30
