import time

import numpy as np
import pytest
from scipy.spatial import distance_matrix
from pair_solver import main


def test_generate_matrix():
    for n in range(10):
        a = np.random.random((10, 3))
        d_mat_example = distance_matrix(a, a)
        test_fn = lambda x, y: np.linalg.norm(x - y)
        np.testing.assert_allclose(main.generate_matrix(a, test_fn), d_mat_example)


@pytest.mark.parametrize(
    "test_input,expected",
    # fmt: off
    [
        pytest.param((np.asarray([0, 1, 2, 3]), {(0, 1), (2, 3)}), 2, id='two_pair_1d_pairing_A'),
        pytest.param((np.asarray([0, 1, 2, 3]), {(0, 3), (1, 2)}), 4, id='two_pair_1d_pairing_B'),
        pytest.param((np.asarray([[0, 3], [4, 0], [-1, -1], [1, 1]]), {(0, 1), (2, 3)}), 5 + 2 * np.sqrt(2), id='two_pair_2d_pairing_A'),
    ],
    # fmt: on
)
def test_evaluate_solution(test_input, expected):
    coords, pairs = test_input
    sq_distance = lambda x, y: np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = main.generate_matrix(coords, sq_distance)
    assert main.evaluate_solution(interaction_matrix, pairs) == expected


@pytest.mark.parametrize(
    # fmt : off
    "test_coords,expected_solution",
    [
        pytest.param(np.array([0, 1]), (set([(0, 1)]), 1), id="one_pair_1d"),
        pytest.param(
            np.array([0, 1, 5, 10]), (set([(0, 1), (2, 3)]), 6), id="two_pair_1d"
        ),
        pytest.param(
            np.array([[0, 10], [1, 10], [5, -12], [10, -12]]),
            (set([(0, 1), (2, 3)]), 6),
            id="two_pair_2d",
        ),
        pytest.param(
            np.array([[0, 10], [1, 10], [5, -12]]),
            None,
            marks=pytest.mark.xfail(
                raises=ValueError, reason="Requires even number of particles"
            ),
            id="odd_pair",
        ),
    ],
)
# fmt : on
def test_pca_sol(test_coords, expected_solution):
    assert main.pca_solution(test_coords) == expected_solution


@pytest.mark.parametrize(
    # fmt : off
    "num_particles, num_dims",
    [
        pytest.param(20, 1, id="small_dataset_1d"),
        pytest.param(20, 3, id="small_dataset_3d"),
        pytest.param(100, 3, id="big_dataset_3d"),
        pytest.param(100, 5, id="big_dataset_5d"),
        # pytest.param(100, 10, id="big_dataset_10d"),
        # pytest.param(1000, 10, id="ultra_dataset_10d"),
    ],
)
# fmt : on
def test_random_solution(num_particles, num_dims, n_reps=5):
    improvement_over_random = []
    for n in range(n_reps):
        test_coords = np.random.rand(num_particles, num_dims)
        soln, pred_loss = main.pca_solution(test_coords)
        _, random_loss = main.random_solution(test_coords)
        print(f"pred_loss = {pred_loss}, random_loss = {random_loss}")
        improvement_over_random.append((random_loss - pred_loss) / random_loss)
    assert np.median(improvement_over_random) > 0.1
