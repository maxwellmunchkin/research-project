import numpy as np
from sklearn.decomposition import PCA


def solution_in_1d(x: np.ndarray) -> set:
    if len(x) % 2 != 0:
        raise ValueError("Can only perform pairing for an even number of particles!")
    arg_sort = np.argsort(x)
    best_pair = set(
        tuple(sorted(arg_sort[i : i + 2])) for i in range(0, len(arg_sort), 2)
    )
    return best_pair


def project_to_1d(x: np.ndarray) -> np.ndarray:
    if len(x) == 1:
        raise ValueError("Cannot project single datapoint into 1D.")

    else:
        if len(x.shape) == 1:
            return x
        pca = PCA(n_components=1)
        transformed = np.ascontiguousarray(pca.fit_transform(x))
        return transformed.flatten()
