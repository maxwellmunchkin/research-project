from typing import Tuple
import random

import numpy as np

from .main import generate_matrix, evaluate_solution, pca_solution


def swap_particles_between_pairs(old_pair: set, swap: tuple) -> set:
    x, y = swap

    sorted_swap_as_a_tuple = tuple(sorted(swap))
    swap_is_in_the_same_pair = sorted_swap_as_a_tuple in old_pair
    if swap_is_in_the_same_pair:
        return old_pair

    swapped_pair = set()
    for pair in old_pair:
        old_particles = set(pair)
        if x in old_particles:
            new_particles = old_particles.copy()
            new_particles.add(y)
            new_particles.discard(x)
        else:
            new_particles = old_particles.copy()
            new_particles.add(x)
            new_particles.discard(y)
        swapped_pair.add(tuple(sorted(new_particles)))
    return swapped_pair


def return_delta_in_loss(interaction_matrix: np.ndarray, old_pair: set, swap) -> Tuple[float, set]:
    swapped_pair = swap_particles_between_pairs(old_pair, swap)
    old_loss = evaluate_solution(interaction_matrix, old_pair)
    new_loss = evaluate_solution(interaction_matrix, swapped_pair)
    delta = new_loss - old_loss
    return delta, swapped_pair


def update_pairing(old_soln: set, old_pair: set, new_pair: set) -> set:
    new_soln = old_soln.copy()
    for pair in old_pair:
        new_soln.remove(pair)
    new_soln = new_soln.union(new_pair)
    return new_soln


def mc_solution(
    coords: np.ndarray,
    budget: int = 1000,
    beta_init: float = 1.0,
    beta_max: float = 100.0,
    num_logs: int = 5,
) -> Tuple[set, float]:
    """Use Monte Carlo with simulated annealing to find an approximate pairing that
    minimises the overall distance between all the pairs

    Args:
        coords (np.ndarray): Coordinates of the input particles, 2NxD
        budget (int, optional): How many MC iterations to run. Defaults to 1000.
        beta_init (float, optional): Starting Boltzmann factor. Low factor leads to more accepted changes. Defaults to 1.0.
        beta_max (float, optional): Maximal Boltzmann factor. Defaults to 100.0.
        num_logs (int, optional): How many messages the solver will output. 0 will not output anything. Defaults to 5.

    Returns:
        Tuple[set, float]: Set containing pairs that minimise the distance, final distance between pairs.
    """

    initial_soln, initial_loss = pca_solution(coords)
    if len(coords.shape) == 1 or coords.shape[1] == 1:
        print("For a 1D case, the optimal solution is sorting.")
        print("Returning the 1D analytical solution.")
        return initial_soln, initial_loss

    # Define the distance function; hardcoded for now
    def sq_distance(x, y): return np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = generate_matrix(coords, sq_distance)

    new_soln = initial_soln.copy()
    new_loss = initial_loss
    num_accepts = 0
    beta = beta_init
    log_freq = budget // num_logs if num_logs > 0 else np.inf
    for step in range(budget):

        two_randomly_picked_pairs = set(random.sample(tuple(new_soln), 2))
        particles_to_be_swapped = {random.sample(
            x, 1)[0] for x in two_randomly_picked_pairs}
        delta, swapped_pair = return_delta_in_loss(
            interaction_matrix, two_randomly_picked_pairs, particles_to_be_swapped)

        randomly_update_even_if_delta_positive = np.exp(
            -1 * delta * beta) > np.random.rand()

        if delta < 0 or randomly_update_even_if_delta_positive:
            new_soln = update_pairing(
                new_soln, two_randomly_picked_pairs, swapped_pair)
            new_loss += delta
            num_accepts += 1

        # Simulated annealing
        if beta < beta_max:
            beta *= 1.02

        if not step % log_freq and step != 0:
            print(f"Step: {step}, acc. ratio: {num_accepts/step:.3f}")

    if num_logs:
        print(f"Ran for {budget} steps, with acc. ratio: {num_accepts/budget}")
        print(f"Initial loss: {initial_loss:.2f}, new_loss: {new_loss:.2f}")
        print(f"Final beta: {beta:.1f}")
    return (new_soln, new_loss)
