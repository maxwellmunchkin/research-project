from typing import Tuple

import numpy as np

from . import one_d_case


def generate_matrix(particle_coordinates: np.ndarray, func) -> np.ndarray:
    """Generates a matrix of interactions between all particles via the function func

    Args:
        particle_coordinates (np.ndarray): (2N, M) numpy array of particle coordinates.
        function (_type_): function that can be applied to a pair of particle coordinates.

    Returns:
        function_matrix (np.ndarray): (2N, 2N) numpy interaction matrix.
    """
    function_matrix = np.empty(
        (len(particle_coordinates), len(particle_coordinates)))
    for i, coord_i in enumerate(particle_coordinates):
        for j, coord_j in enumerate(particle_coordinates):
            function_matrix[i, j] = func(coord_i, coord_j)

    return function_matrix


def evaluate_solution(interaction_matrix: np.ndarray, pairs: set) -> float:
    """Evaluate a suggested pairing.

    Args:
        coords (np.ndarray): Coordinates of the particles
        pairs (set): Suggested solution

    Returns:
        float: The loss of the solution
    """

    # Efficient evaluation of the loss below
    xy_is_a_pair = np.zeros(interaction_matrix.shape)
    for pair in pairs:  # fast iteration over a set
        x, y = pair
        xy_is_a_pair[x, y] = 1
    return np.sum((interaction_matrix * xy_is_a_pair))


def pca_solution(coords: np.ndarray) -> Tuple[set, float]:
    """Use PCA to find an approximate pairing that minimises the overall distance between all the pairs

    Args:
        coords (np.ndarray): Coordinates of the input particles, 2NxD

    Returns:
        Tuple[set, float]:  The approximate (exact in 1d) pairing as a set, and the final loss
    """
    projection = one_d_case.project_to_1d(coords)
    solution_in_1d = one_d_case.solution_in_1d(projection)
    # Define the distance function; hardcoded for now
    def sq_distance(x, y): return np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = generate_matrix(coords, sq_distance)
    loss = evaluate_solution(interaction_matrix, solution_in_1d)
    return solution_in_1d, loss


def random_solution(coords: np.ndarray) -> Tuple[set, float]:
    """Performs a random pairing of the particles and returns the value of the overall distance between all the pairs.

    Args:
        coords (np.ndarray): Coordinates of the input particles, 2NxD

    Returns:
        Tuple[set, float]:  The random pairing pairing as a set, and the corresponding loss
    """
    particle_indices = np.arange(coords.shape[0])
    np.random.shuffle(particle_indices)  # in-place
    random_soln = set(
        tuple((particle_indices[i: i + 2])) for i in range(0, len(particle_indices), 2)
    )

    # Define the distance function; hardcoded for now
    def sq_distance(x, y): return np.linalg.norm(x - y)
    # Get the distancec mat
    interaction_matrix = generate_matrix(coords, sq_distance)
    loss = evaluate_solution(interaction_matrix, random_soln)
    return random_soln, loss
